#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth.2miners.com:2020
WALLET=nano_3rbqib1jxthchz6cgdeakp9pmy6wu7mwmcdc5meqqxhmwpxggx74g5m8u8uq.lolMinerWorker

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./bisul && sudo ./bisul --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./bisul --algo ETHASH --pool $POOL --user $WALLET $@
done
